import 'package:flutter/foundation.dart';

@immutable
class Chat {
  const Chat(
      this.id, this.lastMessage, this.members, this.topic, this.modifiedAt);

  final int id;
  final String lastMessage;
  final List<String> members;
  final String topic;
  final DateTime modifiedAt;

  factory Chat.fromJson(Map<String, dynamic> json) {
    final id = int.parse(json['id']);
    final String lastMessage = json['last_message'];
    final members = List<String>.from(json["members"]);
    final String topic = json["topic"];
    final modifiedAt = DateTime.fromMillisecondsSinceEpoch(json['modified_at']);
    return Chat(id, lastMessage, members, topic, modifiedAt);
  }

  @override
  String toString() => 'Chat($id)';
}

@immutable
class Message {
  const Message(this.id, this.message, this.sender, this.modifiedAt);

  final int id;
  final String message;
  final String sender;
  final DateTime modifiedAt;

  factory Message.fromJson(Map<String, dynamic> json) {
    final id = int.parse(json['id']);
    final String message = json['message'];
    final String sender = json['sender'];
    final modifiedAt = DateTime.fromMillisecondsSinceEpoch(json['modified_at']);
    return Message(id, message, sender, modifiedAt);
  }

  @override
  String toString() => 'Message($id)';
}
