import 'package:flutter/material.dart';
import 'package:idttest/chat_view.dart';
import 'package:idttest/controllers.dart';
import 'package:idttest/entities.dart';
import 'package:idttest/utils.dart';

class InboxView extends StatefulWidget {
  const InboxView(this.controller, {Key? key}) : super(key: key);
  final InboxController controller;
  static const routeName = '/';

  @override
  _InboxViewState createState() => _InboxViewState();
}

class _InboxViewState extends State<InboxView> with AfterFirstBuildMixin {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: widget.controller.isLoading,
      builder: (context, isLoading, _) => Scaffold(
          appBar: AppBar(
            title: const Text('IDT Inbox'),
            actions: const [
              CircularProgressIndicator(),
            ],
          ),
          body: isLoading
              ? const Loading()
              : ValueListenableBuilder<List<Chat>>(
                  valueListenable: widget.controller.chats,
                  builder: (context, chats, _) => ChatList(chats))),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    widget.controller.init();
  }
}

class ChatList extends StatelessWidget {
  const ChatList(this.chats, {Key? key}) : super(key: key);
  final List<Chat> chats;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        final chat = chats[index];
        return ListTile(
            title: Text(chat.members.join(",")),
            subtitle: Text(chat.lastMessage),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ChatView(ChatController(chat)))));
      },
      itemCount: chats.length,
    );
  }
}
