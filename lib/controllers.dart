import 'package:flutter/cupertino.dart';
import 'package:idttest/entities.dart';
import 'package:idttest/services.dart';

class InboxController {
  InboxController();

  final _chats = ValueNotifier<List<Chat>>([]);
  final _isLoading = ValueNotifier<bool>(true);

  get chats => _chats;

  get isLoading => _isLoading;

  Future<void> init() async {
    _isLoading.value = true;
    _chats.value = await ChatService.fetchInbox();
    _isLoading.value = false;
  }
}

class ChatController {
  ChatController(this.chat);

  final Chat chat;
  final _messages = ValueNotifier<List<Message>>([]);
  final _isLoading = ValueNotifier<bool>(true);

  get isLoading => _isLoading;

  get messages => _messages;

  Future<void> init() async {
    _isLoading.value = true;
    _messages.value = await ChatService.fetchChat(chat.id);
    _isLoading.value = false;
  }

  void sendMessage(String text) {
    final msgs = _messages.value;
    final id = msgs.isEmpty ? 1 : msgs.last.id + 1;
    final msg = Message(id, text, 'you', DateTime.now());
    _messages.value = [...msgs, msg];
    ChatService.getResponse(msg).then((received) {
      _messages.value = [..._messages.value, received];
    });
  }
}
