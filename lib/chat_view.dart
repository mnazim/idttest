import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:idttest/controllers.dart';
import 'package:idttest/entities.dart';
import 'package:idttest/utils.dart';

class ChatView extends StatefulWidget {
  const ChatView(this.controller, {Key? key}) : super(key: key);
  final ChatController controller;

  @override
  State<ChatView> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> with AfterFirstBuildMixin {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: widget.controller.isLoading,
      builder: (context, isLoading, _) => Scaffold(
          appBar: AppBar(
            title: Text(widget.controller.chat.topic),
          ),
          body: Column(
            children: [
              Expanded(
                  child: ValueListenableBuilder<List<Message>>(
                      valueListenable: widget.controller.messages,
                      builder: (context, messages, _) =>
                          MessageList(isLoading, messages))),
              Composer(sendMessage: widget.controller.sendMessage),
            ],
          )),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    widget.controller.init();
  }
}

class MessageList extends StatelessWidget {
  const MessageList(this.isLoading, this.messages, {Key? key})
      : super(key: key);
  final bool isLoading;
  final List<Message> messages;

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const Center(child: Loading());
    }
    return ListView.builder(
      itemBuilder: (context, index) {
        final msg = messages[index];
        return MessageTile(msg);
      },
      itemCount: messages.length,
    );
  }
}

class MessageTile extends StatelessWidget {
  const MessageTile(this.message, {Key? key}) : super(key: key);
  final Message message;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 48.0),
      child: Card(
        color: Colors.orange.shade50,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(message.sender),
                  Text('${message.modifiedAt}'),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Text(message.message),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Composer extends StatefulWidget {
  const Composer({Key? key, required this.sendMessage}) : super(key: key);
  final void Function(String) sendMessage;

  @override
  State<Composer> createState() => _ComposerState();
}

class _ComposerState extends State<Composer> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: TextFormField(
        controller: controller,
        maxLines: 3,
        expands: false,
        minLines: 1,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.send,
        decoration: const InputDecoration(
            hintText: "Start typing here!",
            border: InputBorder.none,
            fillColor: Colors.orange,
            contentPadding: EdgeInsets.symmetric(horizontal: 8)),
        onFieldSubmitted: (String line) {
          widget.sendMessage(line);
          controller.clear();
        },
      ),
    );
  }
}
