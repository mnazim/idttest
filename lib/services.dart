import 'dart:convert';
import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:idttest/entities.dart';

class ChatService {
  static const kBaseUrl =
      'https://idtm-media.s3.amazonaws.com/programming-test/api';
  static const kInboxUrl = '$kBaseUrl/inbox.json';

  /// Fix know reason for invalid json text
  static String _fixJsonString(String text) {
    var lastCommaIndex = text.lastIndexOf(',');
    var fixed =
        '${text.substring(0, lastCommaIndex)}${text.substring(lastCommaIndex + 1)}';
    return fixed;
  }

  static Future<List<Map<String, dynamic>>> _getJson(String uri) async {
    final res = await http.get(Uri.parse(uri));
    if (res.statusCode == 200) {
      final text = _fixJsonString(res.body);
      final json = jsonDecode(text);
      return List<Map<String, dynamic>>.from(json);
    }
    return Future.error('${res.statusCode}');
  }

  static Future<List<Chat>> fetchInbox() async {
    final json = await _getJson(kInboxUrl);
    return json.map((e) => Chat.fromJson(e)).toList();
  }

  static Future<List<Message>> fetchChat(int chatId) async {
    final json = await _getJson('$kBaseUrl/$chatId.json');
    return json.map((e) => Message.fromJson(e)).toList().reversed.toList();
  }

  static Future<Message> getResponse(Message message) async {
    await Future.delayed(const Duration(seconds: 1));
    return Message(message.id + Random().nextInt(99999999),
        'You said: "${message.message}"', 'Echo', DateTime.now());
  }
}
