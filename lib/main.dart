import 'package:flutter/material.dart';
import 'package:idttest/controllers.dart';
import 'package:idttest/inbox_view.dart';

void main() {
  final inboxController = InboxController();
  runApp(App(inboxController: inboxController));
}

class App extends StatelessWidget {
  const App({Key? key, required this.inboxController}) : super(key: key);
  final InboxController inboxController;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'IDT programming test',
        theme: ThemeData(primarySwatch: Colors.indigo),
        home: InboxView(inboxController));
  }
}
