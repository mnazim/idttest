# idttest

## Assumptions

- Solve absolute minimum asked in the problem.
- No external dependencies, except http package.
- Remote service will never fail. In production check for things like network availability, service availability, etc.
- Received json will contain valid values. No error checking or schema validation is done.
- No caching implemented.
- No timezone handling.
- No localizations.
- No styling.

To be done:
- Add date/time to inbox view.
- scroll to last message when chat reaches bottom of the screen.
- disable text composer while chat detail is loading


## Architecture

- No complicated architecture; just plain old Flutter!
- Each component is made up of a View and a Controller.
- View implements UI and it's controller implements logic to update the UI.
- Reactive UI built with ValueNotifier and ValueListenableBuild - both provided by flutter.
- Controller classes encapsulate the state within themselves.
- For a more complex interfaces, UI state might be a completely decoupled from it's update logic.
- A single ChatService class is implemented to deal with remote resources.

View -> Controller -> Service